var express = require("express");
var fetch = require("node-fetch");
const util = require("util");
const exec = util.promisify(require("child_process").exec);
var app = express();

async function komutCalistir(komut) {
  const { err, stdout, stderr } = await exec(komut);

  if (err) {
    console.log(err);
    return "error";
  }
  if (stderr) {
    console.log(stderr);
    return "error";
  }
  console.log(stdout);
  return "ok";
}
async function urlKontrol(url) {
  const response = await fetch(url);
  return response.status;
}
async function init() {
  try {
    const url = "https://back.apps.salevali.de/explorer/";

    const urlStatus = await urlKontrol(url);
    console.log(urlStatus);

    if (urlStatus === 502) {
      const ilkKomut = await komutCalistir(
        'cd /var/www/vhosts/back.apps.salevali.de/backend'
      );
      if (ilkKomut === "ok") {
        const ikinciKomut = await komutCalistir(
          'pm2 stop back.apps.salevali.de'
        );
        if (ikinciKomut === "ok") {
          const ucuncuKomut = await komutCalistir(
            'pm2 delete back.apps.salevali.de'
          );
          if (ucuncuKomut === "ok") {
            const sonKomut = await komutCalistir(
              'pm2 start --name back.apps.salevali.de yarn -- start'
            );
            if (sonKomut === "ok") {
              console.log('sistem yeniden baslatildi');
              const sonKomut = await komutCalistir(
                'pm2 list'
              );
            }
          }
        }
      }
    } else {
      console.log("Alles okey");
    }
  } catch (error) {
    console.log(error);
  }
}

var myInt = setInterval(function () {
  init();
}, 60000);

app.listen(1985);
